﻿(function () {
  Backbone.Utils = {
    //loads template from external html file, 
    //must be in /templates folder and have .html extension
    loadTemplateData: function(name) {
  		var url = 'templates/' + name + '.html';
  		var request = $.ajax({
  			url: url
             , dataType: "text"
             , type: "get"
             , cache: "false"
             , async: false
  		});
  		var rez;
  		request.done(function (data) {
  			rez = data;
  		});
  		return rez;
  	},
    //resolves template or its dom node against suplied data
    //name - template name without extension 
    //data - template data
    //selector - dom-node to be selected from template file
  	templateExt: function (name, data, selector) {
  	  var templateHtml = this.loadTemplateData(name);
      var template = selector ? _.unescape($(selector, templateHtml).html()) : templateHtml;
  		var tmplText = _.template(template);
  		return (tmplText)(data);
  	},
    //shows loading wheel on specified dom element (default is body)
    showLoading: function(selector){
      selector = selector || 'body';
      $(selector).addClass('loading');
    },
    //hides loading wheel
    hideLoading: function(selector){
      selector = selector || 'body';
      $(selector).removeClass('loading');
    },
    fixThumbHeight: function(groupSelector, itemSelector) {
      $(itemSelector).height('');
      $(itemSelector).css('padding-top', '');
      _.each($(groupSelector), function(subGroup){
        group = $(itemSelector, subGroup);
        tallest = 0;    
        group.each(function() {       
          thisHeight = $(this).height();       
          if(thisHeight > tallest) {          
              tallest = thisHeight;       
          }    
        });    
        group.each(function() { 
          var paddingTop = (tallest - $(this).height())/2;
          paddingTop = paddingTop < 4 ? 4 : paddingTop;
          $(this).height(tallest); 
          $(this).css('padding-top', paddingTop);
        });
      })
    },
    checkImagesLoaded: function(imgSelector, context, callback, callbackArg1, callbackArg2){
      $(imgSelector, context).one('load', function(){
        var allLoaded = true;
        var images = $(imgSelector, context);
        _.each(images, function(img, index){
          allLoaded = allLoaded && images[index].complete;
        });
        if(allLoaded){callback(callbackArg1, callbackArg2)}
      })
    },
    initMenu: function(){
      $(".toggle-push-right").click(function(e){
          $(e.target).hasClass('close-menu') 
              ? Backbone.Utils.hideMenu() 
              : Backbone.Utils.showMenu();
      });
    },
    showMenu: function(){
        $('body').append("<div class='mask'><div>").toggleClass('pmr-open', true).css('overflow', 'hidden');
        $('.toggle-push-right').toggleClass('close-menu', true);
        $('.mask').click(Backbone.Utils.hideMenu);
    },
    hideMenu: function(){
        $('.mask').remove();
        $('body').toggleClass('pmr-open', false).css('overflow', '');
        $('.toggle-push-right').toggleClass('close-menu', false);
    }
  }
})()