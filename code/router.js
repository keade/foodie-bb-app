﻿$(function () {
    var AppRouter = Backbone.Router.extend({
        routes : {
            "": "posts",
            "post/:id": "postDetails",
            "category/:slug" : "posts",
            "postSearch/:search" : "postSearch"
        },
        posts : function(category) {
            this.loadView(new Backbone.AppViews.Posts({
                category: category
            }));
        },
        postSearch: function(search){
            this.loadView(new Backbone.AppViews.Posts({
                search: search
            }));
        },
        postDetails: function(id){
            this.loadView(new Backbone.AppViews.PostDetails({
                id: id
            }));
        },
        loadView : function(view) {
            $('#footer').remove();
            this.view && this.view.remove();
            this.view = view;
        }
    }); 

    var app_router = new AppRouter;
    Backbone.AppData = {};
    Backbone.history.start();

    //init attach categories to menu
    (new Backbone.AppViews.Categories().render());

    //search init, attach events
    function goSearch(){
        var searchTerm = $('#k').val();
        Backbone.AppData.SearchVisible = false;
        window.location.hash = '#postSearch/' + encodeURIComponent(searchTerm);
    }

    $('.search').click(function(){
        Backbone.Utils.hideMenu();
        var searchDiv = $('.global-search');
        if(Backbone.AppData.SearchVisible){
          searchDiv.slideUp();
          $('#s').unbind('click');
        }
        else{
          $('body').scrollTop(0);
          searchDiv.slideDown();
          $('#s').click(goSearch);
        }
        Backbone.AppData.SearchVisible = !Backbone.AppData.SearchVisible;
    });

    $(window).on('hashchange', function(hash){
        Backbone.Utils.hideMenu();
    });

    Backbone.Utils.initMenu();
});