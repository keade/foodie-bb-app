﻿Backbone.AppViews = {};

Backbone.AppViews.Posts = Backbone.View.extend({
  initialize: function(options) {
    this.category = options.category;
    this.search = options.search;
    $(".content").html(this.el);
    this.models = new Backbone.Model.Models();
    this.render();
  },
  render: function(){
    Backbone.Utils.showLoading();
    this.models.getLatest(this.renderPosts, {
      category: this.category,
      search: this.search
    }, this);
    $('.postNav').hide();
    this.$el.html(Backbone.Utils.templateExt('Posts'));
    return this;
  },
  renderPosts: function(data){
    //splitting posts array to smaller arrays (2 post in one) 
    var j = 0,
        row = [],
        rows = [];
    for(var i = 0; i < data.posts.length; i++){
      if(i != 0 && i % 2 == 0){
        rows.push(row);
        row = [];
        j = 0;
      }
      row.push(data.posts[i]);
    }
    if(row.length){
      rows.push(row);
    }
    $('.content').append(Backbone.Utils.templateExt('PostsContent', {data: rows}));
    Backbone.Utils.checkImagesLoaded('img', $('.content'), Backbone.Utils.fixThumbHeight, '.content .row', '.thumbnail');
    $(window).on('resize', function(){
      Backbone.Utils.fixThumbHeight('.content .row', '.thumbnail');
    })
    Backbone.Utils.hideLoading();
    var view = this;
    $('.loadMore').click(function(){
      view.loadMore();
    });
    return this;
  },
  loadMore: function(){
    Backbone.AppData.PostsOptions.page = Backbone.AppData.PostsOptions.page + 1;
    this.models.getLatest(this.renderPosts, Backbone.AppData.PostsOptions, this);
    Backbone.Utils.showLoading();
    $('.loadMore').remove();
  }
});

Backbone.AppViews.Categories = Backbone.View.extend({
  initialize: function() {
    this.models = new Backbone.Model.Models();
    this.render();
  },
  render: function(){
    this.models.getCategories(this.renderCategories);
  },
  renderCategories: function(data){
    _.each(data.categories, function(category){
      var item = $('<a>')
        .text(category.name)
        .attr('href', '#category/' + category.slug)
      var li = $('<li>');

      $('.push-menu-right ul').append(li.append(item));
      Backbone.Utils.hideLoading();
    })
  }
});

Backbone.AppViews.PostDetails = Backbone.View.extend({
  initialize: function(options) {
    $(".content").html(this.el);
    this.models = new Backbone.Model.Models();
    this.setNavigation(options.id);
    this.render(options.id);
  },
  render: function(id){
    $('.postNav').show();
    this.$el.html(Backbone.Utils.templateExt('PostDetails'));
    this.models.getPost(this.renderPost, id);
    Backbone.Utils.showLoading();
  },
  renderPost: function(postData){
    var title = $('<h3>').html(postData.title);
	var author = $('<h4>').html(postData.author.name);
    $('.content').append(title).append(author).append(postData.content);
	$('img').addClass('img-responsive');
	$('.wp-caption').addClass('img-responsive img-capt');
    var footer = '<div id="footer" class="navbar navbar-fixed-bottom"> \
                    <div class="container"> \
                      Footer text \
                    </div> \
                  </div>' ;
    $('body').append(footer);
    Backbone.Utils.hideLoading();
  },
  setNavigation: function(id){
    var navUrls = this.models.getPrevNext(parseInt(id));
    $('.postNav .prev').attr('href', navUrls.prevUrl);
    $('.postNav .next').attr('href', navUrls.nextUrl);
    $('.prev').parent().toggleClass('disabled', !navUrls.prevUrl);
    $('.next').parent().toggleClass('disabled', !navUrls.nextUrl);
  }
});
