Backbone.Model.Models = Backbone.Model.extend({
  apiBaseUrl: 'https://public-api.wordpress.com/rest/v1/sites/afoodieworld.com/',

  constructPostsRequest: function(options){
    var url = this.apiBaseUrl + 'posts';
    options.page = options.page || 1;
    url += "?page=" + options.page;
    if(options.category){
      url += "&category=" + options.category;
    }
    if(options.search){
      url+= "&search=" + encodeURIComponent(options.search);
    }
    Backbone.AppData.PostsOptions = options;
    return url;
  },  

  getLatest: function (callback, options, context) {
    $.get(this.constructPostsRequest(options), function(data){
      Backbone.AppData.Posts = _.pluck(data.posts, 'ID');
      Backbone.AppData.PostsInCategory = data.found;
      Backbone.AppData.PostsLoaded = data.posts.length + (options.page - 1)*20;
      callback.apply(context, [data]);
    })
  },
  
  getCategories: function(callback){
    $.get(this.apiBaseUrl + 'categories', function(categories){
      callback(categories);
    })
  },

  getPost: function(callback, id){
    $.get(this.apiBaseUrl + 'posts/' + id, function(post){
      callback(post);
    })
  },
  getPrevNext: function(id){
    var prevUrl, nextUrl;
    if(Backbone.AppData.Posts){
      var curPos = Backbone.AppData.Posts.indexOf(id);
      if(curPos >= 0){
        prevUrl = curPos > 0 
            ? '#post/' + Backbone.AppData.Posts[curPos - 1]
            : '';
        nextUrl = curPos < Backbone.AppData.Posts.length - 1 
            ? '#post/' + Backbone.AppData.Posts[curPos + 1]
            : '';
      }
    }
    return {
      prevUrl: prevUrl,
      nextUrl: nextUrl
    }
  }
})